% clear;
% clc;
close;

%% Set Parameters for Loading Data
fig=figure;
set(fig,'DefaultAxesFontSize',20);
set(fig,'DefaultAxesFontWeight','bold');

data_root = '';
color_list = linspecer(4);
BW=125000;
SF=7;

% nelora_file='evaluation/sf7_v1_';
% nelora_file='evaluation/sf7_125k_new_';
% improved_nelora_file='evaluation/sf7_traineval5e-4_';
nelora_file='evaluation/sf7_v4_';
improved_nelora_file='evaluation/sf7_v2.3_maskcnn_no_mask_';
prune_nelora_file='evaluation/prune_sf7_v1_';
baseline_file='evaluation/baseline_error_matrix_';

% nelora_file='evaluation/sf7_v3_';
% nelora_file='evaluation/sf7_traineval5e-4_';
SNR_list=[-40:15];
SNR_improved_list=[-40:15];
SNR_prone_list=[-40:15];
SNR_list_baseline=[-30:0];

% improved nelora
name_str=[improved_nelora_file,num2str(SF),'_',num2str(BW),'.mat'];
error_path = [data_root,name_str];
a2 = load(error_path);
error_matrix = a2.error_matrix;
error_rate = 1-error_matrix;
plot(SNR_improved_list,error_rate,"-.*",'LineWidth',3,'color',color_list(3,:));
hold on;

% nelora
name_str=[nelora_file,num2str(SF),'_',num2str(BW),'.mat'];
error_path = [data_root,name_str];
a = load(error_path);
error_matrix = a.error_matrix;
error_rate = 1-error_matrix;
plot(SNR_list,error_rate,"-.*",'LineWidth',3,'color',color_list(1,:));
hold on;

% prone
name_str=[prune_nelora_file,num2str(SF),'_',num2str(BW),'.mat'];
error_path = [data_root,name_str];
a = load(error_path);
error_matrix = a.error_matrix;
error_rate = 1-error_matrix;
plot(SNR_prone_list,error_rate,"-.*",'LineWidth',2,'color',color_list(4,:));
hold on;

% baseline
name_str=[baseline_file,num2str(SF),'_',num2str(BW),'V1.mat'];
error_path = [data_root,name_str];
a = load(error_path);
error_matrix = a.error_matrix;
plot(SNR_list_baseline,1-error_matrix,"-.*",'LineWidth',2,'color',color_list(2,:));
hold on;

% legend('Imroved NeLoRA', 'NeLoRA', 'Prone', 'Baseline')
% legend('NeLoRA', 'Baseline')
% legend('Imroved NeLoRA', 'NeLoRA', 'Baseline')
% legend('Imroved NeLoRA', 'NeLoRA', 'Baseline')
legend('Imroved NeLoRA', 'NeLoRA', 'Prone', 'Baseline')

% legend('abs_baselineNELoRa','Baseline')
xlabel('SNR (dB)'); % x label
ylabel('SER'); % y label
title('Decode SER for SF=7')
xlim([-20,-10]);
set(gcf,'WindowStyle','normal','Position', [200,200,640,360]);
grid on;
saveas(gcf,[data_root,'res/',num2str(SF),'_',num2str(BW),'.pdf'])
