# models.py

import torch
import torch.nn as nn
import torch.nn.functional as F


class classificationHybridModel(nn.Module):
    """Defines the architecture of the discriminator network.
       Note: Both discriminators D_X and D_Y have the same architecture in this assignment.
    """

    def __init__(self, conv_dim_in=2, conv_dim_out=128, conv_dim_lstm=1024):
        super(classificationHybridModel, self).__init__()

        self.out_size = conv_dim_out
        self.conv1 = nn.Conv2d(conv_dim_in, 16, (3, 3), stride=(2, 2), padding=(1, 1))
        self.pool1 = nn.MaxPool2d((2, 2), stride=(2, 2))
        self.dense = nn.Linear(conv_dim_lstm * 4, conv_dim_out * 4)
        self.fcn1 = nn.Linear(conv_dim_out * 4, conv_dim_out * 2)
        self.fcn2 = nn.Linear(2 * conv_dim_out, conv_dim_out)
        self.softmax = nn.Softmax(dim=1)

        self.drop1 = nn.Dropout(0.2)
        self.drop2 = nn.Dropout(0.5)
        self.act = nn.ReLU()
        self._initialize_weights()

    def _initialize_weights(self):
        # import ipdb; ipdb.set_trace()
        for module in self.modules():
            if isinstance(module, nn.Linear):
                nn.init.kaiming_normal_(module.weight, mode='fan_in')
                if module.bias is not None:
                    nn.init.constant_(module.bias, 0)
                    
    def forward(self, x): # 8, 2, 128, 33
        out = self.act(self.conv1(x)) # 8, 16, 64, 17
        out = self.pool1(out) # 8, 16, 32, 8
        out = out.reshape(out.size(0), -1) # 

        out = self.act(self.dense(out))
        out = self.drop2(out)

        out = self.act(self.fcn1(out))
        out = self.drop1(out)
        out = self.fcn2(out)
        return out


class maskCNNModel(nn.Module):
    def __init__(self, opts):
        super(maskCNNModel, self).__init__()
        self.opts = opts

        self.conv = nn.Sequential(
            # cnn1
            nn.ZeroPad2d((3, 3, 0, 0)),
            nn.Conv2d(opts.x_image_channel, 64, kernel_size=(1, 7), dilation=(1, 1)),
            nn.BatchNorm2d(64), nn.ReLU(),

            # cnn2
            nn.ZeroPad2d((0, 0, 3, 3)),
            nn.Conv2d(64, 64, kernel_size=(7, 1), dilation=(1, 1)),
            nn.BatchNorm2d(64), nn.ReLU(),

            # cnn3
            nn.ZeroPad2d(2),
            nn.Conv2d(64, 64, kernel_size=(5, 5), dilation=(1, 1)),
            nn.BatchNorm2d(64), nn.ReLU(),

            # cnn4
            nn.ZeroPad2d((2, 2, 4, 4)),
            nn.Conv2d(64, 64, kernel_size=(5, 5), dilation=(2, 1)),
            nn.BatchNorm2d(64), nn.ReLU(),

            # cnn5
            nn.ZeroPad2d((2, 2, 8, 8)),
            nn.Conv2d(64, 64, kernel_size=(5, 5), dilation=(4, 1)),
            nn.BatchNorm2d(64), nn.ReLU(),

            # cnn6
            nn.ZeroPad2d((2, 2, 16, 16)),
            nn.Conv2d(64, 64, kernel_size=(5, 5), dilation=(8, 1)),
            nn.BatchNorm2d(64), nn.ReLU(),

            # cnn7
            nn.ZeroPad2d((2, 2, 32, 32)),
            nn.Conv2d(64, 64, kernel_size=(5, 5), dilation=(16, 1)),
            nn.BatchNorm2d(64), nn.ReLU(),

            # cnn8
            nn.Conv2d(64, 8, kernel_size=(1, 1), dilation=(1, 1)),
            nn.BatchNorm2d(8), nn.ReLU(),

        )
        
        self.lstm = nn.LSTM(
            opts.conv_dim_lstm,
            opts.lstm_dim,
            batch_first=True,
            bidirectional=True
        )

        self.fc1 = nn.Linear(2 * opts.lstm_dim, opts.fc1_dim)
        self.fc2 = nn.Linear(opts.fc1_dim, opts.freq_size * opts.y_image_channel)
        self._initialize_weights() #initialize with kaiming initialization for all the linear layers

    def _initialize_weights(self):
        # import ipdb; ipdb.set_trace()
        for module in self.modules():
            if isinstance(module, nn.Linear):
                nn.init.kaiming_normal_(module.weight, mode='fan_in')
                if module.bias is not None:
                    nn.init.constant_(module.bias, 0)
                    
    def forward(self, x): # x: 8, 2, 128, 33
        out = x.transpose(2, 3).contiguous()
        out = self.conv(out) # out: 8, 8, 33, 128
        out = out.transpose(1, 2).contiguous()  # out: 8, 33, 8, 128
        out = out.view(out.size(0), out.size(1), -1) # out: 8, 33, 1024
        out, _ = self.lstm(out) # out: 8, 33, 800
        out = F.relu(out)
        out = self.fc1(out) # out: 8, 33, 600
        out = F.relu(out)
        out = self.fc2(out) # out: 8, 33, 256

        out = out.view(out.size(0), out.size(1), self.opts.y_image_channel, -1) # out: 8, 33, 2, 128
        # out = torch.sigmoid(out) # 
        out = out.transpose(1, 2).contiguous() # out: 8, 2, 33, 128
        out = out.transpose(2, 3).contiguous() # out: 8, 2, 128, 33
        masked = out
        # masked = out * x  # out is mask, masked is denoised
        return masked


class ResidualBlock(nn.Module):
    def __init__(self, channels):
        super(ResidualBlock, self).__init__()
        self.conv1 = nn.Conv2d(channels, channels, kernel_size=3, padding=1)
        self.bn1 = nn.BatchNorm2d(channels)
        self.conv2 = nn.Conv2d(channels, channels, kernel_size=3, padding=1)
        self.bn2 = nn.BatchNorm2d(channels)

    def forward(self, x):
        identity = x

        out = F.relu(self.bn1(self.conv1(x)))
        out = self.bn2(self.conv2(out))

        out += identity
        out = F.relu(out)

        return out


class AEResidualBlock(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size=3, stride=1, padding=1):
        super(AEResidualBlock, self).__init__()
        self.conv1 = nn.Conv2d(in_channels, out_channels, kernel_size, stride, padding)
        self.bn1 = nn.BatchNorm2d(out_channels)
        self.conv2 = nn.Conv2d(out_channels, out_channels, kernel_size, stride, padding)
        self.bn2 = nn.BatchNorm2d(out_channels)
        self.relu = nn.ReLU(inplace=True)

        self.skip = nn.Sequential()
        if stride != 1 or in_channels != out_channels:
            self.skip = nn.Sequential(
                nn.Conv2d(in_channels, out_channels, 1, stride),
                nn.BatchNorm2d(out_channels)
            )

    def forward(self, x):
        identity = self.skip(x)

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)

        out += identity
        out = self.relu(out)

        return out

class DenoisingNet(nn.Module):
    def __init__(self, opts=None):
        super(DenoisingNet, self).__init__()
        self.initial_conv = nn.Conv2d(2, 64, kernel_size=3, padding=1)
        self.res_block1 = ResidualBlock(64)
        self.res_block2 = ResidualBlock(64)
        # self.res_block3 = ResidualBlock(64)
        # self.res_block4 = ResidualBlock(64)
        self.final_conv = nn.Conv2d(64, 2, kernel_size=3, padding=1)

    def forward(self, x):
        output = F.relu(self.initial_conv(x))
        output = self.res_block1(output)
        output = self.res_block2(output)
        # output = self.res_block3(output)
        # output = self.res_block4(output)
        output = self.final_conv(output)
        mask = output.sigmoid()
        output = mask * x
        return output

class Autoencoder(nn.Module):
    def __init__(self):
        super(Autoencoder, self).__init__()

        self.encoder = nn.Sequential(
            AEResidualBlock(2, 32),
            AEResidualBlock(64, 64),
            AEResidualBlock(64, 128)
        )
        self.decoder = nn.Sequential(
            AEResidualBlock(128, 64),
            AEResidualBlock(64, 32),
            AEResidualBlock(32, 2)
        )

    def forward(self, x):
        x = self.encoder(x)
        x = self.decoder(x)
        return x

if __name__ == '__main__':
    # create mo
    # model = DenoisingNet()
    model = Autoencoder()

    # Input data
    batch_size = 8
    dummy_input = torch.randn(batch_size, 2, 128, 33)

    output = model(dummy_input)
    print(output.shape)  # [5, 2, 128, 33]
