import numpy
import scipy.io
import torch
import torch.nn.utils.prune as prune
import torch.quantization
from rich.progress import track

import end2end
import utils


def apply_pruning_to_model(model):
    for name, module in model.named_modules():
        if isinstance(module, torch.nn.Conv2d) or isinstance(module, torch.nn.Linear):
            prune.l1_unstructured(module, 'weight', amount=0.3)
            prune.remove(module, 'weight')
    return model


def go_prune(opts, v_x_l, v_y_l):
    print(f'Pruning & Quantization')

    # assert load pre-trained parameters
    assert opts.load, "Please load the pre-trained model before pruning."

    # load model
    model_mask_cnn, model_classification = end2end.load_checkpoint(opts)
    model_mask_cnn.eval()
    model_classification.eval()

    # apply pruning
    model_mask_cnn_pruned = apply_pruning_to_model(model_mask_cnn)
    model_classification_pruned = apply_pruning_to_model(model_classification)

    # apply quantization
    model_mask_cnn_pruned.qconfig = torch.quantization.get_default_qconfig('fbgemm')
    model_classification_pruned.qconfig = torch.quantization.get_default_qconfig('fbgemm')
    torch.quantization.prepare(model_mask_cnn_pruned, inplace=True)
    torch.quantization.prepare(model_classification_pruned, inplace=True)

    # calibration
    t_x, t_y = iter(v_x_l), iter(v_y_l)
    iter_per_epoch = min(len(v_x_l), len(v_y_l), 500)

    error_m = numpy.zeros([len(opts.snr_list), 1], dtype=float)
    error_m_cnt = numpy.zeros([len(opts.snr_list), 1], dtype=int)
    error_m_info = []

    # iterate forward
    saved_data = {}
    for it_idx in track(range(500), description='Calibration Progress'):
        # load x data
        img_x, name_x = next(t_x)

        # mappings
        code_x = [float(i.split('_')[0]) for i in name_x]
        snr_x = [int(i.split('_')[1]) for i in name_x]
        instance_x = [int(i.split('_')[4]) for i in name_x]
        label_x = [int(x.split('_')[5]) for x in name_x]

        img_x_v, label_x_v = utils.to_var(img_x, True), utils.to_var(torch.tensor(label_x), True)

        # load y data
        img_y_v, label_y_v = next(t_y)
        img_y_v = utils.to_var(img_y_v, True)

        i_x_spectrum_r = torch.stft(
            input=img_x_v, n_fft=opts.stft_nfft,
            hop_length=opts.stft_overlap, win_length=opts.stft_window,
            pad_mode='constant',
        )
        i_x_spectrum = utils.spec_to_network_input(i_x_spectrum_r, opts)

        i_y_spectrum_r = torch.stft(
            input=img_y_v, n_fft=opts.stft_nfft,
            hop_length=opts.stft_overlap, win_length=opts.stft_window,
            pad_mode='constant',
        )
        i_y_spectrum = utils.spec_to_network_input(i_y_spectrum_r, opts)

        # fake & label
        fake_y_spectrum = model_mask_cnn_pruned(i_x_spectrum)
        label_x_est = model_classification_pruned(fake_y_spectrum)
        saved_sample = utils.to_data(label_x_est)

        # count save data
        for i, label in enumerate(utils.to_data(label_x_v)):
            if label not in saved_data.keys():
                saved_data[label] = []
                saved_data[label].append(saved_sample[i])
            else:
                saved_data[label].append(saved_sample[i])
        _, label_x_v_est = torch.max(label_x_est, 1)

        test_right_c = utils.to_data(label_x_v_est == label_x_v, True)

        # calculate error
        for batch in range(min(opts.batch_size, len(snr_x))):
            snr_idx = opts.snr_list.index(snr_x[batch])
            error_m[snr_idx] += test_right_c[batch]
            error_m_cnt[snr_idx] += 1
            error_m_info.append(
                [
                    instance_x[batch], code_x[batch],
                    snr_x[batch],
                    label_x_v_est[batch].cpu().data.int(),
                    label_x_v[batch].cpu().data.int()
                ]
            )

    # save error matrix
    error_m = numpy.divide(error_m, error_m_cnt)
    error_m_info = numpy.array(error_m_info)
    scipy.io.savemat(
        opts.root_path + '/' + opts.dir_comment + '_' + str(opts.sf) + '_' + str(opts.bw) + '.mat',
        dict(
            error_matrix=error_m,
            error_matrix_count=error_m_cnt,
            error_matrix_info=error_m_info,
        )
    )

    # finish quantization
    torch.quantization.convert(model_mask_cnn_pruned, inplace=True)
    torch.quantization.convert(model_classification_pruned, inplace=True)

    # save quantized model
    torch.save(
        model_mask_cnn_pruned.state_dict(),
        f'{opts.root_path}/{opts.dir_comment}_{opts.sf}_{opts.bw}_mask_cnn_pruned.pth'
    )
    torch.save(
        model_classification_pruned.state_dict(),
        f'{opts.root_path}/{opts.dir_comment}_{opts.sf}_{opts.bw}_classification_pruned.pth'
    )
