
python main.py --dir_comment sf7_v2.4_AE \
               --batch_size 16 \
               --lr 0.0005 \
               --root_path . \
               --data_dir /home/liyifa11/MyCodes/AIoT891/project1/sf7_125k \
               --groundtruth_code 35 \
               --normalization \
               --train_iter 100000 \
               --log_step 2000 \
               --ratio_bt_train_and_test 0.8 \
               --network end2end_debug \
               --free_gpu_id 1

